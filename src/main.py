from dash import dcc, html
from dash.dependencies import Input, Output

import essaiWebUI
import globalWebUI
from app import app
from componentWebUI import navbar

choice_machine = ""


def serve_layout():
    return html.Div([
        dcc.Location(id='url', refresh=False),
        navbar,
        html.Div(id='page-content', children=[])
    ])


app.layout = serve_layout


# callback pour mettre à jour les pages


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/homepage':
        return globalWebUI.layout_index
    elif pathname == '/dashboard':
        return essaiWebUI.layout_machine
    else:
        return globalWebUI.layout_index


if __name__ == '__main__':
    app.run_server(host='0.0.0.0')
