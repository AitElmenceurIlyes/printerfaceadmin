import json

from dash import html
from dash.dependencies import Input, Output, State

from app import app
from componentWebUI import Machine, getmonitor, input_connexion, machines_card

layout_index = html.Div([
    input_connexion,
    html.Br(),
    html.Div(machines_card, id="machines_card")
])


@app.callback(
    Output('machines_card', 'children'),
    [Input('add-button', 'n_clicks'),
     State('name', 'value'),
     State('hostname', 'value'),
     State('username', 'value'),
     State('password', 'value')]
)
def add_monitor(n, name, hostname, username, password):
    if n and name is not None and hostname is not None and username is not None and password is not None:
        temp = getmonitor()
        temp.append({
            "name": name,
            "hostname": hostname,
            "port": "22",
            "username": username,
            "password": password,
        })
        Machine(name, hostname, username, password).addmachine()
        with open('config.json', 'w', encoding='utf-8') as file:
            file.write(json.dumps(temp))
    return machines_card
