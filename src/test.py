import unittest

import utils.connexionSSH as Sh


class ConnectionSSHTest(unittest.TestCase):
    def test_connection(self):
        # Test of the connection from SSH
        hostnameright = "sshserver"
        hostnamewrong = "255.2S55.255.255"
        usernameright = "test"
        usernamewrong = "username"
        passwordright = "test"
        passwordwrong = "password"
        client = Sh.SSHConnect('sshserver', 'test', 'test')
        self.assertEqual(client.hostname, hostnameright)
        self.assertNotEqual(client.hostname, hostnamewrong)
        self.assertEqual(client.username, usernameright)
        self.assertNotEqual(client.username, usernamewrong)
        self.assertEqual(client.password, passwordright)
        self.assertNotEqual(client.password, passwordwrong)
        # self.assertNotEquals(client.client, None)
        self.assertEqual(
            Sh.SSHConnect(
                hostnameright,
                usernameright,
                passwordwrong).client,
            None)

    def test_ram(self):
        client = Sh.SSHConnect('sshserver', 'test', 'test')
        ram = client.ram()
        self.assertEqual(len(ram.split(' ')), 3)
        self.assertNotEqual(len(ram.split(' ')), None)

    def test_net(self):
        client = Sh.SSHConnect('sshserver', 'test', 'test')
        net = client.net()
        self.assertEqual(len(net.splitlines()[2:]), 2)
        self.assertNotEqual(len(net.splitlines()[2:]), None)

    def test_hd(self):
        client = Sh.SSHConnect('sshserver', 'test', 'test')
        hd = client.hd()
        self.assertEqual(len(hd.split()[7:]), 6)
        self.assertNotEqual(len(hd.split()[7:]), None)

    def test_cpu(self):
        client = Sh.SSHConnect('sshserver', 'test', 'test')
        cpu = client.cpu()
        self.assertNotEqual(cpu, None)


if __name__ == '__main__':
    unittest.main()
