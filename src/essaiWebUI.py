from datetime import datetime, timedelta

import dash_component_unload as duc
import plotly
import plotly.graph_objs as go
from dash import html
from dash.dependencies import Input, Output
from dash.exceptions import PreventUpdate

import utils.InfoGetter as Getter
from app import app
from componentWebUI import (dcc_intervals, getmachinewithname, graph_cpu, graph_get, graph_hd, graph_net,
                            graph_ram, graph_visit)

date_ram = []
ram_value = []
date_cpu = []
cpu_value = []
date_net = []
max_ram = int(0)
net_received_value = []
net_emitted_value = []
getter = Getter.InfoGetter()
currentMachine = None

# Layout dashboard machine
layout_machine = html.Div(children=[
    html.H2(className='form-label mt-4',
            children=' ',
            style={'font-size': '1.5em'},
            id="machineName"),
    html.Br(),
    dcc_intervals,
    html.Br(),
    graph_cpu,
    graph_ram,
    graph_net,
    graph_visit,
    graph_hd,
    graph_get,

    duc.DashComponentUnload(
        id='before_unload',
    ),
    html.Div(id='output')
])


@app.callback(
    Output('machineName', 'children'),
    [Input('url', 'search')]
)
def display_value(search):
    if search is None:
        return ''
    else:
        print("Connecting to " + search[1:])
        current_machine = getmachinewithname(search[1:])
        date_ram.clear()
        ram_value.clear()
        date_cpu.clear()
        cpu_value.clear()
        date_net.clear()
        net_received_value.clear()
        net_emitted_value.clear()
        getter.connect(current_machine.hostname,
                       current_machine.username, current_machine.password)
        print("Connected to " + search[1:])

    return ("Details from machine '" + search[1:] + "'")


@app.callback(
    Output('output', 'children'),
    [Input('before_unload', 'close')]
)
def display_output(close):
    if not close:
        raise PreventUpdate
    if getter.isconnected():
        print("déconnexion")
        getter.disconnect()
        date_ram.clear()
        ram_value.clear()
        date_cpu.clear()
        cpu_value.clear()
        date_net.clear()
        net_received_value.clear()
        net_emitted_value.clear()
    return str(close)


@app.callback(
    Output("refresh-date", "children"),
    [Input('interval-live-update', 'n_intervals')],
)
def display_value(value):
    return 'Refresh at {}'.format(datetime.now())


# Affichage du texte et stockage des valeurs
# RAM
@app.callback(
    Output("text-ram", "children"),
    [Input('interval-live-update', 'n_intervals')],
)
def display_ram(value):
    if len(date_ram) > 15:
        date_ram.pop(0)
        ram_value.pop(0)
    getter.refresh()
    if getter.isconnected():
        now = datetime.now()
        now.strftime("%d/%m/%Y %H:%M:%S")
        date_ram.append(now)

        ram_value.append(getter.get_ram_used())
        global max_ram
        max_ram = int(getter.get_ram_total())

        return 'Ram total : {} mB - Ram free : {} mB - Ram used : {} mB'.format(
            round(int(getter.get_ram_total()) / 1000, 1),
            round(int(getter.get_ram_free()) / 1000, 1),
            round(int(getter.get_ram_used()) / 1000, 1)
        )
    else:
        return 'Ram usage : Offline'


# NET


@app.callback(
    Output("text-net", "children"),
    [Input('interval-live-update', 'n_intervals')],
)
def display_net(value):
    if len(date_net) > 15:
        date_net.pop(0)
        net_received_value.pop(0)
        net_emitted_value.pop(0)
    if getter.isconnected():

        received_temp = getter.ge_net_details("eth0:", "received", "packets")
        emitted_temp = getter.ge_net_details("eth0:", "emitted", "packets")

        if (received_temp is not None and emitted_temp is not None):
            now = datetime.now()
            now.strftime("%d/%m/%Y %H:%M:%S")
            date_net.append(now)
            net_received_value.append(received_temp)
            net_emitted_value.append(emitted_temp)
            return 'Received packets : {} - Emitted packets : {}'.format(received_temp, emitted_temp)
        else:
            return 'Received packets : {} - Emitted packets : {}'.format(0, 0)
    else:
        return 'Packets count : Offline'


@app.callback(
    Output("text-cpu", "children"),
    [Input('interval-live-update', 'n_intervals')],
)
def display_cpu(value):
    if len(date_cpu) > 15:
        date_cpu.pop(0)
        cpu_value.pop(0)
    if getter.isconnected():
        received_cpu = getter.get_cpu()
        if received_cpu != "None":
            received_cpu = received_cpu.replace("%", "").replace("\n", "")
            now = datetime.now()
            now.strftime("%d/%m/%Y %H:%M:%S")
            date_cpu.append(now)
            cpu_value.append(received_cpu)
            return 'CPU Usage : {}% '.format(int(float(received_cpu)))
        else:
            return 'CPU Usage : Unknown '
    else:
        return 'CPU Usage : Offline '


# Mise à jour des graphiques
# RAM

@app.callback(
    Output('graph-ram', 'figure'),
    [Input('interval-live-update', 'n_intervals')],
)
def update_ramgraph(n):
    if getter.isconnected() and len(date_ram) > 0:
        data = plotly.graph_objs.Scatter(
            x=list(date_ram),
            y=list(ram_value),
            name='Used RAM',
            mode='lines+markers'
        )
        shapes = list()
        shapes.append({'type': 'line',
                       'xref': 'x',
                       'yref': 'y',
                       'x0': min(date_ram),
                       'y0': max_ram,
                       'x1': max(date_ram),
                       'y1': max_ram})

        return {'data': [data],
                'layout': go.Layout(shapes=shapes, xaxis=dict(title='RAM Usage',
                                                              range=[max(date_ram) - timedelta(minutes=1),
                                                                     max(date_ram)]),
                                    yaxis=dict(range=[int(0), int(max_ram + 50)]))}
    return {'data': []}


# NET


@app.callback(
    Output('graph-net', 'figure'),
    [Input('interval-live-update', 'n_intervals')],
)
def update_netgraph(n):
    if getter.isconnected() and len(date_ram) > 0:
        data = plotly.graph_objs.Scatter(
            x=list(date_net),
            y=list(net_received_value),
            name='Received packets',
            mode='lines+markers'
        )
        data2 = plotly.graph_objs.Scatter(
            x=list(date_net),
            y=list(net_emitted_value),
            name='Sent packets',
            mode='lines+markers'
        )

        return {'data': [data, data2],
                'layout': go.Layout(title='Packets transmitted',
                                    xaxis=dict(range=[max(date_net) - timedelta(minutes=1), max(date_net)]),
                                    yaxis=dict(range=[int(0), int(float(max(max(net_received_value), max(net_received_value))) * float(1.2))]))}
    return {'data': []}


# Visits


@app.callback(
    Output('graph-visits', 'figure'),
    [Input('interval-slow-update', 'n_intervals')],
)
def update_visitsgraph(n):
    if getter.isconnected():
        y = getter.get_logs()
        if y == "-1":
            return {'data': []}
        url = []
        nb_visiteurs = []
        for key in y["StatsPages"]:
            value = y["StatsPages"][key]
            if (value > 1):
                nom = (key[:30] + '..') if len(key) > 30 else key
                url.append(nom)
                nb_visiteurs.append(value)

    else:
        url = []
        nb_visiteurs = []
        url.append("Not connected!")
        nb_visiteurs.append(1)
    data = plotly.graph_objs.Pie(labels=url, values=nb_visiteurs)

    return {'data': [data]}


# HD


@app.callback(
    Output('graph-hd', 'figure'),
    [Input('interval-slow-update', 'n_intervals')],
)
def update_hdgraph(n):
    if getter.isconnected():
        liste_hd = getter.get_hd_list()
        if (liste_hd is None):
            type = []
            space = []
            type.append("Not connected!")
            space.append(1)
        else:
            dd1 = liste_hd[0]
            if (dd1 is not None):
                hd_percentage = getter.get_hd_details(str(dd1), "use_percentage")

                hd_percentage = hd_percentage.replace("%", "")

                type = []
                space = []

                type.append("Occupied")
                space.append(hd_percentage)

                type.append("Free")
                space.append(100 - int(hd_percentage))
            else:
                type = []
                space = []
                type.append("Not connected!")
                space.append(1)
    else:
        type = []
        space = []
        type.append("Not connected!")
        space.append(1)
    data = plotly.graph_objs.Pie(labels=type, values=space)

    return {'data': [data]}


# connexions GET


@app.callback(
    Output('graph-connections-count', 'figure'),
    [Input('interval-slow-update', 'n_intervals')],
)
def update_getgraph(n):
    if getter.isconnected():
        y = getter.get_logs()
        if y == "-1":
            return {'data': []}
        data = plotly.graph_objs.Scatter(
            x=list(y["listeDates"]),
            y=list(y["listValeursGet"]),
            name='Successful requests',
            mode='lines'
        )
        data2 = plotly.graph_objs.Scatter(
            x=list(y["listeDates"]),
            y=list(y["listValeurs404"]),
            name='Bad requests',
            mode='lines',
            yaxis='y2'
        )
        return {'data': [data, data2],
                'layout': go.Layout(xaxis=dict(range=[min(y["listeDates"]), max(y["listeDates"])]),
                                    yaxis=dict(title='Status = 200', range=[
                                        int(0), max(y["listValeursGet"])]),
                                    yaxis2=dict(title='Status = 404',
                                                range=[int(0), max(int(1), max(y["listValeurs404"]))], overlaying='y',
                                                side='right'))}
    else:
        return {'data': []}


# CPU


@app.callback(
    Output('graph-cpu', 'figure'),
    [Input('interval-live-update', 'n_intervals')],
)
def update_cpuraph(n):
    if getter.isconnected() and len(date_cpu) > 0:
        data = plotly.graph_objs.Scatter(
            x=list(date_cpu),
            y=list(cpu_value),
            name='Scatter',
            mode='lines+markers'
        )

        return {'data': [data],
                'layout': go.Layout(
                    xaxis=dict(title='CPU Usage', range=[max(date_cpu) - timedelta(minutes=1), max(date_cpu)]),
                    yaxis=dict(range=[int(0), int(108)]))}
    return {'data': []}
