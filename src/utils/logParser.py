#!/usr/bin/env python3

import apache_log_parser
import pandas as pd


class LogParser:

    def analyse_logs(self):
        line_parser_access_log = apache_log_parser.make_parser(
            "%h %l %u %t \"%r\" %>s %b")
        comptage_ips = {}
        comptage_pages = {}
        nb_404 = 0
        nb_requetes = 0
        graph_temporel_errors404 = []
        graph_temporel_get = []

        for line in self.splitlines():
            # print(line)
            log_line_data = {}
            try:
                log_line_data = line_parser_access_log(line)
                # pprint(log_line_data)
                if log_line_data['remote_host'] in comptage_ips:
                    comptage_ips[log_line_data['remote_host']] = comptage_ips[log_line_data['remote_host']] + 1
                else:
                    comptage_ips[log_line_data['remote_host']] = 1

                if log_line_data['request_url_path'] in comptage_pages:
                    comptage_pages[log_line_data['request_url_path']] += 1
                else:
                    comptage_pages[log_line_data['request_url_path']] = 1
                if log_line_data['status'] == '404':
                    nb_404 += 1
                    graph_temporel_errors404.append(
                        log_line_data['time_received_datetimeobj'])
                if log_line_data['request_method'] == 'GET':
                    nb_requetes += 1
                    graph_temporel_get.append(
                        log_line_data['time_received_datetimeobj'])
            except:
                True  # print("ERROR parsing '" + line + "'")

        subdivisions = 30

        graph_temporel_errors404.sort()
        if not graph_temporel_errors404 or not graph_temporel_get:
            # Erreur, une des deux listes (ou les deux) est vide
            print("Error getting logs!")
            return "-1"
        list_dates = pd.date_range(min(min(graph_temporel_errors404), min(graph_temporel_get)), max(max(
            graph_temporel_errors404), max(graph_temporel_get)), periods=subdivisions).tolist()
        list_valeurs404 = [0] * subdivisions
        list_valeurs_get = [0] * subdivisions

        i = 0
        for date in graph_temporel_errors404:
            if list_dates[i] < date:
                if date < list_dates[i + 1]:
                    list_valeurs404[i] += 1
                else:
                    i += 1

        j = 0
        for date in graph_temporel_get:
            if list_dates[j] < date:
                if date < list_dates[j + 1]:
                    list_valeurs_get[j] += 1
                else:
                    j += 1
        list_valeurs_get[j] += 1

        demi_pas = (list_dates[1] - list_dates[0]) / 2

        liste_date_datetime = []
        for date in list_dates:
            liste_date_datetime.append((date + demi_pas).to_pydatetime())

        resultat = {"StatsIP": comptage_ips,
                    "StatsPages": comptage_pages,
                    "Errors404": nb_404,
                    "listeDates": liste_date_datetime,
                    "listValeursGet": list_valeurs_get,
                    "listValeurs404": list_valeurs404}
        return resultat
