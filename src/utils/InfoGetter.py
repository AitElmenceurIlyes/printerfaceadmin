import utils.connexionSSH as Sh
from utils.logParser import LogParser


class InfoGetter:
    def __init__(self, ip=None, username=None, password=None):
        # '161.3.160.65','interfadm','Projet654!'
        self.ip = ip
        self.username = username
        self.password = password
        self.client = None
        self.ram = None
        self.net = None
        self.log = None
        self.hd = None
        self.cpu = None
        self.timeout = False
        if self.ip != None:
            self.client = Sh.SSHConnect(ip, username, password)
            self.refresh()

    def isconnected(self):
        if self.timeout == True:
            return False
        if self.client == None:
            return False
        return self.client.is_connected()

    def connect(self, ip, username, password):
        self.ip = ip
        self.username = username
        self.password = password
        self.client = Sh.SSHConnect(self.ip, self.username, self.password)
        self.refresh()
        return self.client.is_connected()

    def disconnect(self):
        self.ip = None
        self.username = None
        self.password = None
        if self.client.is_connected():
            self.client.disconnect()

    def refresh(self):
        self.timeout = False
        if self.client is not None:
            if self.client.is_connected() == True:
                self.ram = self.client.ram()
                if self.ram == "-1":
                    self.timeout = True
                self.net = self.client.net()
                if self.net == "-1":
                    self.timeout = True
                self.log = self.client.dl_apache_logs()
                if self.log == "-1":
                    self.timeout = True
                self.hd = self.client.hd()
                if self.hd == "-1":
                    self.timeout = True
                self.cpu = self.client.cpu()
                if self.cpu == "-1":
                    self.timeout = True

    def refreshmain(self):
        self.timeout = False
        if self.client is not None:
            if self.client.is_connected() == True:
                self.ram = self.client.ram()
                if self.ram == "-1":
                    self.timeout = True
                self.cpu = self.client.cpu()
                if self.cpu == "-1":
                    self.timeout = True
            # else:
            #     self.connect(self.ip, self.username, self.password)

    def get_ram_total(self):
        try:
            return self.ram.splitlines()[0].split()[0]
        except:
            return "0"

    def get_ram_free(self):
        try:
            return self.ram.splitlines()[0].split()[2]
        except:
            return "0"

    def get_ram_used(self):
        try:
            return self.ram.splitlines()[0].split()[1]
        except:
            return "0"

    def get_net_list(self):
        liste = []
        for line in self.net.splitlines()[2:]:
            liste.append(line.split(' ')[0])
        return liste
        # return net.splitlines()[2].split(' ')[0]

    # Direction: received or transmitted
    # Type: bytes or packets
    def ge_net_details(self, interface, direction, type):
        liste = []
        for line in self.net.splitlines()[2:]:
            if line.split(' ')[0] == interface:
                if (direction == "received"):
                    if (type == "packets"):
                        return line.split(' ')[2]
                    else:
                        return line.split(' ')[1]
                else:
                    if (type == "packets"):
                        return line.split(' ')[4]
                    else:
                        return line.split(' ')[3]

    # Hard Drive
    def get_hd_list(self):
        liste = []
        for line in self.hd.splitlines()[1:]:
            liste.append(line.split(' ')[0])
        return liste

    def get_hd_details(self, hdname, data):
        liste = []
        for line in self.hd.splitlines()[1:]:
            if line.split(' ')[0] == hdname:
                if (data == "used"):
                    return line.split(' ')[2]
                if (data == "available"):
                    return line.split(' ')[3]
                if (data == "use_percentage"):
                    return line.split(' ')[4]
                if (data == "mounted"):
                    return line.split(' ')[5]

    def get_cpu(self):
        return str(self.cpu)

    def get_logs(self):
        return LogParser.analyse_logs(self.log)
