#!/usr/bin/env python3


from paramiko import AutoAddPolicy, SSHClient, ssh_exception


class SSHConnect:
    def __init__(self, hostname, username, pwd):
        self.client = None
        self.hostname = hostname
        self.username = username
        self.password = pwd
        self.connect()

    def connect(self):
        try:
            self.client = SSHClient()
            self.client.load_system_host_keys()
            self.client.set_missing_host_key_policy(AutoAddPolicy)
            self.client.connect(hostname=self.hostname,
                                username=self.username, password=self.password, banner_timeout=3.0, auth_timeout=3.0,
                                timeout=3.0)
        except ssh_exception.AuthenticationException:
            print("Authentication failed, please verify your credentials")
            self.client.close()
            self.client = None
        except ssh_exception.SSHException as sshexception:
            print("Unable to establish SSH connection")
            self.client.close()
            self.client = None
        except TimeoutError:
            print(
                "A connection attempt failed because the connected party did not respond properly beyond a certain time or an established connection failed because the connection host did not respond")
            self.client.close()
            self.client = None
        except Exception:
            self.client = None

    def disconnect(self):
        if self.client:
            self.ip = None
            self.username = None
            self.password = None
            self.client.close()

    def is_connected(self):
        transport = self.client.get_transport() if self.client else None
        return transport and transport.is_active()

    def ram(self):
        try:
            _, stdout, stderr = self.client.exec_command(
                "free | head -n 2 | tail -n 1 | tr -s ' ' | cut -d' ' -f 2-4")
            ram = stdout.read().decode("utf-8")
            return ram
        except Exception:
            print("Error : unable to get RAM information")
            return "-1"

    def net(self):
        try:
            _, stdout, stderr = self.client.exec_command(
                "cat /proc/net/dev | tr -s ' ' | cut -d' ' -f 2-4,11-12")
            output = stdout.read().decode("utf-8")
            return output
        except Exception:
            print("Error : unable to get Net information")
            return "-1"

    def hd(self):
        try:
            _, stdout, stderr = self.client.exec_command("df / | tr -s ' '")
            output = stdout.read().decode("utf-8")
            return output
        except Exception:
            print("Error : unable to get HD information")
            return "-1"

    def cpu(self):
        try:
            _, stdout, stderr = self.client.exec_command(
                "awk '{u = $2+$4; t = $2+$4+$5; if (NR == 1){u1 = u; t1 = t;} else print ($2+$4-u1) * 100 / (t-t1) \"%\"; }' <(grep 'cpu ' /proc/stat) <(sleep 0.5;grep 'cpu ' /proc/stat)")
            # awk '{u = $2+$4; t = $2+$4+$5; if (NR == 1){u1 = u; t1 = t;} else print ($2+$4-u1) * 100 / (t-t1) "%"; }' <(grep 'cpu ' /proc/stat) <(sleep 0.5;grep 'cpu ' /proc/stat)
            # grep 'cpu ' /proc/stat | awk '{usage = ($2+$4)*100/($2+$4+$5)} END {print usage "%"}'
            cpu = stdout.read().decode("utf-8")
            return cpu
        except Exception:
            print("Error : unable to get CPU information")
            return "-1"

    def dl_apache_logs(self):
        try:
            _, stdout, stderr = self.client.exec_command(
                "cat /var/log/apache2/access.log")
            output = stdout.read().decode("utf-8")
            if (output == ""):
                _, stdout, stderr = self.client.exec_command(
                    "cat /var/log/apache2/other_vhosts_access.log")
                output = stdout.read().decode("utf-8")
            if (output == ""):
                _, stdout, stderr = self.client.exec_command(
                    "cat /var/log/apache2/other_vhosts_access.log.1")
                output = stdout.read().decode("utf-8")
            if (output == ""):
                print("ERREUR lors du téléchargement des logs (pas de logs)")
            return output
        except Exception as ex:
            print("Error : unable to get Apache access log information: " + str(ex))
            return "-1"
