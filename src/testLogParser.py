import pickle
import unittest

from utils.logParser import LogParser


class ParserTest(unittest.TestCase):
    def test_parser(self):
        self.maxDiff = None
        with open("log.txt", 'r', encoding="utf-8") as log, open("log.pickle", 'rb') as pickle_log:
            parsed = pickle.load(pickle_log)
            lines = log.read()
            log_parsed = LogParser.analyse_logs(lines)

            self.assertEqual(log_parsed, parsed)
        # pickle.dump(log_parsed, open("log.pickle", "wb")) # To generate pickle test on an ok example


if __name__ == '__main__':
    unittest.main()
