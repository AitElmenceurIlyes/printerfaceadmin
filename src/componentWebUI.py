import json

import dash_bootstrap_components as dbc
from dash import dcc, html
from dash.dependencies import MATCH, Input, Output, State

import utils.InfoGetter as Getter
from app import app

#
# from paramiko.ssh_exception import SSHException


# Menu déroulant avec choix MonitorMe1 et MonitorMe2


def getmonitorname():
    monitor = []
    with open('config.json', 'r', encoding='utf-8') as json_data:
        data_dict = json.load(json_data)
        for x in data_dict:
            monitor.append(x["name"])
    return monitor


def getmonitor():
    with open('config.json', 'r', encoding='utf-8') as json_data:
        data_dict = json.load(json_data)
        return data_dict


machines_card = [
    dcc.Interval(
        id='connections-refresh',
        interval=5 * 1000,  # in milliseconds
        n_intervals=0
    )
]


class Machine:
    def __init__(self, name, hostname, username, password):
        self.name = name
        self.hostname = hostname
        self.username = username
        self.password = password
        self.getter = None

    def connect(self):
        self.getter = Getter.InfoGetter()
        self.getter.connect(
            self.hostname, self.username, self.password)

    def addmachine(self):
        self.displaymachine()
        machines.append(self)

    def displaymachine(self):
        machines_card.append(dbc.Card(
            dbc.CardBody([
                dbc.Row([
                    dbc.Col([
                        html.H4(self.name)
                    ],
                        width=2
                    ),
                    dbc.Col([
                        html.Div(
                            id={
                                'type': 'udStatus',
                                'index': self.name
                            },
                            children="Down"
                        )],
                        width=1
                    ),
                    dbc.Col([
                        html.Div(
                            id={
                                'type': 'cpuStatus',
                                'index': self.name
                            },
                            children=" "
                        )],
                        width=2
                    ),
                    dbc.Col([
                        html.Div(
                            id={
                                'type': 'ramStatus',
                                'index': self.name
                            },
                            children=" "
                        )],
                        width=2
                    ),
                    dbc.Col([
                        dbc.Button("See details", id=self.name, external_link=True,
                                   color="primary", href="/dashboard?" + self.name)
                    ],
                        width=3
                    )
                ])
            ])
        ))

        try:
            self.connect()
        except Exception as err:
            print("Error connecting to " + self.name)
            print(Exception, err)


machines = []


def getmachinewithname(name):
    return next((x for x in machines if x.name == name), None)


for x in getmonitor():
    Machine(x["name"], x["hostname"], x["username"],
            x["password"]).addmachine()

# Input de connexion aux machines distantes
input_connexion = html.Div(children=[
    html.H3(className='form-label mt-4',
            children='Add a new machine',
            style={'font-size': '1.5em'}),
    dbc.Row([
        dbc.Col([
            dbc.Label("Machine name", html_for="hostname"),
            dbc.Input(className='form-control', id='name', value='',
                      type='text'),
        ],
            width=3,
        ),
        dbc.Col([
            dbc.Label("Hostname", html_for="hostname"),
            dbc.Input(className='form-control', id='hostname', value='',
                      type='text'),
        ],
            width=3,
        ),
        dbc.Col([
            dbc.Label("Username", html_for="username"),
            dbc.Input(className='form-control', id='username',
                      value='', type='text'),
        ],
            width=3,
        ),
        dbc.Col([
            dbc.Label("Password", html_for="password"),
            dbc.Input(className='form-control', id='password',
                      value='', type='password'),
        ],
            width=2,
        ),
        dbc.Col([
            dbc.Button("+", color="primary", className="me-2",
                       id="add-button"),
        ],
            width=1,
            style={'margin-top': '50px'}
        )],
        className="g-3",
    ),
])

# Bouton refresh
dcc_intervals = html.Div(children=[
    dcc.Interval(
        id='interval-live-update',
        interval=5 * 1000,  # in milliseconds
        n_intervals=0
    ),
    dcc.Interval(
        id='interval-slow-update',
        interval=10 * 1000,  # in milliseconds
        n_intervals=0
    ),
    html.Div(id='refresh-date'),
])

# Graph affichant la RAM
graph_ram = html.Div(children=[
    html.Div(id='text-ram'),
    dcc.Graph(id='graph-ram', animate=True),

], style={'display': 'inline-block', 'width': '50%'})

graph_net = html.Div(children=[
    html.Div(id='text-net'),
    dcc.Graph(id='graph-net', animate=True),

], style={'display': 'inline-block', 'width': '50%'})

graph_visit = html.Div(children=[
    html.Div(id='text-visits', children='Visited pages repartition'),
    dcc.Graph(id='graph-visits'),

], style={'display': 'inline-block', 'width': '50%'})

graph_hd = html.Div(children=[
    html.Div(id='text-hd', children='Hard drive usage'),
    dcc.Graph(id='graph-hd'),

], style={'display': 'inline-block', 'width': '50%'})
PLOTLY_LOGO = "https://images.plot.ly/logo/new-branding/plotly-logomark.png"

navbar = dbc.Navbar(
    dbc.Container(
        [
            dbc.Row(
                [
                    dbc.Col(),
                    dbc.Col(dbc.Button("Home Page", href="/homepage",
                                       external_link=True, color="primary"),
                            width=3),
                    dbc.Col(dbc.NavbarBrand("Projet Interface Administration",
                                            className="ms-2", style={'font-size': '2em'}),
                            width=4),
                ],
                align="center",
                className="g-0",
            ),
            dbc.Col(),
        ],
    ),
    color="#333333",
    dark=True,
)

# Graph affichant les connexions GET
graph_get = html.Div(children=[
    html.Div(id='text-get', children='Connetions count'),
    dcc.Graph(id='graph-connections-count', animate=True),
], style={'display': 'inline-block', 'width': '50%'})

# Graph affichant l'utilisation du CPU
graph_cpu = html.Div(children=[
    html.Div(id='text-cpu'),
    dcc.Graph(id='graph-cpu', animate=True),
], style={'display': 'inline-block', 'width': '50%'})

navbarhomepage = dbc.Navbar(
    dbc.Container([
        html.A(
            dbc.Row([
                dbc.NavbarBrand("Projet Interface Administration",
                                className="ms-2", style={'font-size': '2em'}),
            ],
                align="center",
                className="g-0",
            ),
        )
    ]),
    color="#333333",
    dark=True,
)


@app.callback(
    Output({'type': 'udStatus', 'index': MATCH}, 'children'),
    Input('connections-refresh', 'n_intervals'),
    State({'type': 'udStatus', 'index': MATCH}, 'id'),
)
def update_ud(n, id):
    machine = getmachinewithname(id['index'])
    if machine is not None:
        machine.getter.refreshmain()
        if machine.getter.isconnected():
            return "Up"
        else:
            return "Down"
    else:
        return "error getting machine " + str(id['index'])


@app.callback(
    Output({'type': 'cpuStatus', 'index': MATCH}, 'children'),
    Input('connections-refresh', 'n_intervals'),
    State({'type': 'cpuStatus', 'index': MATCH}, 'id'),
)
def update_cpu(n, id):
    machine = getmachinewithname(id['index'])
    if machine is not None and machine.getter.isconnected():
        return "CPU used : " + machine.getter.get_cpu()
    else:
        return " "


@app.callback(
    Output({'type': 'ramStatus', 'index': MATCH}, 'children'),
    Input('connections-refresh', 'n_intervals'),
    State({'type': 'ramStatus', 'index': MATCH}, 'id'),
)
def update_ram(n, id):
    machine = getmachinewithname(id['index'])
    if machine is not None and machine.getter.isconnected():
        return 'Ram used : {} mB '.format(round(int(machine.getter.get_ram_used()) / 1000, 1))
    else:
        return " "
