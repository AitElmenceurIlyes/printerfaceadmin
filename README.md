# Administration interface project

## TSE > INFORX > [Projet interface administration](https://mootse.telecom-st-etienne.fr/course/view.php?id=940) > *group10*

A server and web interface to administer a set of Apache servers, without any installation on client machines.

A simple ssh login with the appropriate desired rights is required.



## Installation :

To use this app, simply run

```sh
docker pull ilyesaitel/interfaceadmin:deploy
docker run -d -p 8050:8050/tcp ilyesaitel/interfaceadmin:deploy
```

Then go to the [web interface](http://localhost:8050/) with your favourite web browser



## Build :

In order to build the docker image, run

```sh
docker build --pull --rm -f "docker\client\Dockerfile" -t ilyesaitel/interfaceadmin:client "docker\client"
```

